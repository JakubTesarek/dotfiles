# find printers

    lpstat -a

# set sdefault printer

    lpoptions -d E5PKonicaMinoltaBizhubC224
    lpoptions -d Canon_MF8000_Series

# print

    lpr file.pdf
    lpr -o fit-to-page -o media=A4 image.png
