my history:
-----------
**distro:**

~1/2009		Ubuntu
~11/2009	Xubuntu
8/2010		Debian

**de:**

~1/2009	  GNOME 2
~11/2009  Xfce
~6/2014   Awesome
3/2020    i3
4/2020    Sway

**browser:**

IE –2004
Firefox 2004–2006
Opera 2006–2013
Firefox 2013–2018
qutebrowser 2018–

**editor:**
vim ~2013–


desktops:
---------
1.	pracovní (třeba build skripty)
2.	web/im
3.	mimopracovní (třeba awesome konfig)
4.	db
5.	ssh
6.	2. prohlížeč / gimp / inkscape
7.	workers
8.	backend
9.	frontend
