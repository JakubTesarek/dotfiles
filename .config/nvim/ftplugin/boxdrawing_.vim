" mnemonics: 
" 	West ╸, North ╹, East ╺, South ╻
" 	Horizontal ━, Vertical ┃
" 	R ┏, I ┓, L ┗, J ┛
" 	 F ┣, 4 ┫, T ┳, A ┻
" 	  X ╋

" WNES
abbreviate <buffer> ╴╴ ╴
abbreviate <buffer> ╵╵ ╵
abbreviate <buffer> ╶╶ ╶
abbreviate <buffer> ╷╷ ╷
abbreviate <buffer> ╸╸ ╸
abbreviate <buffer> ╹╹ ╹
abbreviate <buffer> ╺╺ ╺
abbreviate <buffer> ╻╻ ╻

" H
abbreviate <buffer> ── ─
abbreviate <buffer> ╴╺ ╼
abbreviate <buffer> ╺╴ ╼
abbreviate <buffer> ╸╶ ╾
abbreviate <buffer> ╶╸ ╾
abbreviate <buffer> ━━ ━

" V
abbreviate <buffer> ││ │
abbreviate <buffer> ╵╻ ╽
abbreviate <buffer> ╻╵ ╽
abbreviate <buffer> ╹╷ ╿
abbreviate <buffer> ╷╹ ╿
abbreviate <buffer> ┃┃ ┃

" R
abbreviate <buffer> ┌┌ ┌

abbreviate <buffer> ╷╺ ┍
abbreviate <buffer> ╺╷ ┍
abbreviate <buffer> ╻╶ ┎
abbreviate <buffer> ╶╻ ┎

abbreviate <buffer> ┏┏ ┏

" I
abbreviate <buffer> ┐┐ ┐

abbreviate <buffer> ╷╸ ┑
abbreviate <buffer> ╸╷ ┑
abbreviate <buffer> ╻╴ ┒
abbreviate <buffer> ╴╻ ┒

abbreviate <buffer> ┓┓ ┓

" L
abbreviate <buffer> └└ └

abbreviate <buffer> ╵╺ ┕
abbreviate <buffer> ╺╵ ┕
abbreviate <buffer> ╹╶ ┖
abbreviate <buffer> ╶╹ ┖

abbreviate <buffer> ┗┗ ┗

" J
abbreviate <buffer> ┘┘ ┘

abbreviate <buffer> ╵╸ ┙
abbreviate <buffer> ╸╵ ┙
abbreviate <buffer> ╹╴ ┚
abbreviate <buffer> ╴╹ ┚

abbreviate <buffer> ┛┛ ┛

" F
abbreviate <buffer> ├├ ├

abbreviate <buffer> ╹┌ ┞
abbreviate <buffer> ┌╹ ┞
abbreviate <buffer> ╺│ ┝
abbreviate <buffer> │╺ ┝
abbreviate <buffer> ╻└ ┟
abbreviate <buffer> └╻ ┟

abbreviate <buffer> ╷┗ ┡
abbreviate <buffer> ┗╷ ┡
abbreviate <buffer> ╶┃ ┠
abbreviate <buffer> ┃╶ ┠
abbreviate <buffer> ╵┏ ┢
abbreviate <buffer> ┏╵ ┢

abbreviate <buffer> ┣┣ ┣

" 4
abbreviate <buffer> ┤┤ ┤

abbreviate <buffer> ╸│ ┥
abbreviate <buffer> │╸ ┥
abbreviate <buffer> ╹┐ ┦
abbreviate <buffer> ┐╹ ┦
abbreviate <buffer> ╻┘ ┧
abbreviate <buffer> ┘╻ ┧

abbreviate <buffer> ╷┛ ┩
abbreviate <buffer> ┛╷ ┩
abbreviate <buffer> ╵┓ ┪
abbreviate <buffer> ┓╵ ┪
abbreviate <buffer> ╴┃ ┨
abbreviate <buffer> ┃╴ ┨

abbreviate <buffer> ┫┫ ┫

" T
abbreviate <buffer> ┬┬ ┬

abbreviate <buffer> ╸┌ ┭
abbreviate <buffer> ┌╸ ┭
abbreviate <buffer> ╺┐ ┮
abbreviate <buffer> ┐╺ ┮
abbreviate <buffer> ╷━ ┯
abbreviate <buffer> ━╷ ┯

abbreviate <buffer> ╻─ ┰
abbreviate <buffer> ─╻ ┰
abbreviate <buffer> ╶┓ ┱
abbreviate <buffer> ┓╶ ┱
abbreviate <buffer> ╴┏ ┲
abbreviate <buffer> ┏╴ ┲

abbreviate <buffer> ┳┳ ┳

" A
abbreviate <buffer> ┴┴ ┴

abbreviate <buffer> ╸└ ┵
abbreviate <buffer> └╸ ┵
abbreviate <buffer> ╹─ ┸
abbreviate <buffer> ─╹ ┸
abbreviate <buffer> ╺┘ ┶
abbreviate <buffer> ┘╺ ┶

abbreviate <buffer> ╶┛ ┹
abbreviate <buffer> ┛╶ ┹
abbreviate <buffer> ╵━ ┷
abbreviate <buffer> ━╵ ┷
abbreviate <buffer> ╴┗ ┺
abbreviate <buffer> ┗╴ ┺

abbreviate <buffer> ┻┻ ┻

" X
abbreviate <buffer> ┼┼ ┼

abbreviate <buffer> ╸├ ┽
abbreviate <buffer> ├╸ ┽
abbreviate <buffer> ╹┬ ╀
abbreviate <buffer> ┬╹ ╀
abbreviate <buffer> ╺┤ ┾
abbreviate <buffer> ┤╺ ┾
abbreviate <buffer> ╻┴ ╁
abbreviate <buffer> ┴╻ ╁

abbreviate <buffer> ─┃ ╂
abbreviate <buffer> ┃─ ╂
abbreviate <buffer> ┛┌ ╃
abbreviate <buffer> ┌┛ ╃
abbreviate <buffer> ┐┗ ╄
abbreviate <buffer> ┗┐ ╄
abbreviate <buffer> ┓└ ╅
abbreviate <buffer> └┓ ╅
abbreviate <buffer> ┘┏ ╆
abbreviate <buffer> ┏┘ ╆
abbreviate <buffer> ━│ ┿
abbreviate <buffer> │━ ┿

abbreviate <buffer> ╷┻ ╇
abbreviate <buffer> ┻╷ ╇
abbreviate <buffer> ╶┫ ╉
abbreviate <buffer> ┫╶ ╉
abbreviate <buffer> ╵┳ ╈
abbreviate <buffer> ┳╵ ╈
abbreviate <buffer> ╴┣ ╊
abbreviate <buffer> ┣╴ ╊

abbreviate <buffer> ╋╋ ╋
