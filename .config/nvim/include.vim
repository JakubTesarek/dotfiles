call plug#begin('~/.config/nvim/bundle')
" LANGUAGE SUPPORT
" WEB
" Plug 'ternjs/tern_for_vim', { 'for': 'javascript' }		" js completer
" Plug 'carlitux/deoplete-ternjs', { 'for': 'javascript' }
" Plug 'leafgarland/typescript-vim', { 'for': 'typescript' }	" ts completer
Plug 'ap/vim-css-color'
" Plug 'digitaltoad/vim-pug', { 'for': ['pug', 'jade'] }	" jade highlighting
Plug 'kevinoid/vim-jsonc'
Plug 'MaxMEllon/vim-jsx-pretty', { 'for': 'jsx' }		" jsx
" PYTHON
" Plug 'nvie/vim-flake8', { 'for': 'python' }			" PEP8 check
Plug 'hynek/vim-python-pep8-indent', { 'for': 'python' }
" Plug 'rdnetto/YCM-Generator', { 'branch': 'stable', 'for': ['c', 'cpp'] }
" --clang-completer --system-boost --system-libclang
" Plug 'Valloric/YouCompleteMe', { 
" 			\'do': 'python3 install.py --clang-completer --system-boost --system-libclang --cs-completer --ts-completer',
" 			\'for': ['c', 'cpp', 'python', 'go', 'typescript', 'javascript', 'rust'] }
Plug 'neoclide/coc.nvim', { 'branch': 'release' }
" Plug 'davidhalter/jedi-vim', { 'for': 'python' }
" Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
" Plug 'deoplete-plugins/deoplete-jedi', { 'for': 'python' }
Plug 'Konfekt/FastFold'
Plug 'tmhedberg/SimpylFold', { 'for': 'python' }
" C#
" Plug 'OmniSharp/omnisharp-vim', { 'for': 'cs' }
" RUBY
" Plug 'vim-ruby/vim-ruby', { 'for': 'ruby' }
" Plug 'tpope/vim-rails', { 'for': 'ruby' }

" DOCS
" Plug 'gerw/vim-latex-suite', { 'for': 'tex' }
" Plug 'lervag/vimtex', { 'for': 'tex' }
Plug '/usr/share/lilypond/2.18.2/vim/', { 'for': 'lilypond' }
" Plug 'dbakker/vim-rstlink', { 'for': 'rst' }
" Plug 'greyblake/vim-preview', { 'for': 'markdown' }
" Plug 'suan/vim-instant-markdown', { 'for': 'markdown' }
" Plug 'torkve/vim-markdown-extra-preview', { 'for': 'markdown' }
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() } }
" VIM
Plug 'tpope/vim-scriptease', { 'for': 'vim' }	" for vim plugins
Plug 'Shougo/neco-vim', { 'for': 'vim' }	" viml completion
" DB
" Plug 'vim-scripts/dbext.vim', { 'for': 'sql' }	" db access
Plug 'tpope/vim-dadbod'
Plug 'kristijanhusak/vim-dadbod-completion', { 'for': 'sql' }	" db completion
" PS
" Plug 'PProvost/vim-ps1', { 'for': 'ps1' }	" powershell highlighting
" CSV, TSV
Plug 'mechatroner/rainbow_csv'

" SYNTAX
Plug 'vim-syntastic/syntastic'			" syntax checking
Plug 'vim-scripts/matchit.zip'			" `%`
Plug 'tpope/vim-surround'			" quoting
Plug 'tpope/vim-commentary'			" `gcc`
Plug 'ervandew/supertab'			" `<tab>`
Plug 'tpope/vim-sleuth'				" adjust shiftwidth
Plug 'christoomey/vim-titlecase'		" gt
Plug 'tommcdo/vim-lion'				" align `glip=`
Plug 'AndrewRadev/sideways.vim'			" swap parameters

" IMAGES
Plug 'tpope/vim-afterimage'

" VCS
Plug 'tpope/vim-fugitive'			" git
Plug 'tommcdo/vim-fubitive'			" bitbucket for fugitive
" Plug 'airblade/vim-gitgutter'			" git diff in sign col
" Plug 'junegunn/gv.vim'			" commit browser
" Plug 'airblade/vim-rooter'			" cd to project dir
Plug 'dbakker/vim-projectroot'

" COMMANDS
" Plug 'tpope/vim-unimpaired'			" ][
" Plug 'tommcdo/vim-ninja-feet'			" c]i)
Plug 'tpope/vim-eunuch'				" shell commands
Plug 'antoyo/vim-licenses'
" Plug 'godlygeek/tabular'
Plug 'dhruvasagar/vim-table-mode'
Plug 'vim-scripts/BufOnly.vim'
Plug 'tpope/vim-repeat'				" . works for plugins
Plug 'tpope/vim-speeddating'
" Plug 'itchyny/calendar.vim' 
Plug 'christoomey/vim-sort-motion'		" sorting with gs
Plug 'tsuyoshicho/vim-pass'
" Plug 'tpope/vim-dispatch'			" async make
Plug 'tommcdo/vim-exchange'			" exchange motion, cxaw

" LOOK AND FEEL
Plug 'morhetz/gruvbox'				" color theme
Plug 'vim-airline/vim-airline'
Plug 'osyo-manga/vim-anzu'			" search position
Plug 'junegunn/vim-peekaboo'			" show content of registers
" Plug 'johngrib/vim-game-code-break'		" arkanoid
call plug#end()
