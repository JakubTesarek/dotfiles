" <Leader> with comma, <LocalLeader> with backslash
let mapleader = ','
let maplocalleader = '\'

" free keys:
" =		!+
" y[] 		Q
" dg		
" zc		Z

" BASICS {{{1
noremap ; :
noremap : ;
noremap Y y$
nnoremap <silent> <ESC> :cclose<BAR>nohlsearch<CR>
map Q <nop>
map R <nop>
" map U <nop>

" MOVING AROUND {{{1
noremap <Space> <C-d>
noremap <BS> <C-u>

" TABS {{{1
noremap ZZ :update<BAR>bdelete<CR>
noremap ZA :%update<BAR>%bdelete<CR>
noremap ZO :%update<BAR>BufOnly<CR>
noremap ZQ :bdelete!<CR>

noremap <C-k> :bprevious<CR>
noremap <C-j> :bnext<CR>
noremap ó :bprevious<CR>
noremap π :bnext<CR>
noremap <C-ESC> :buffer#<CR>
noremap <C-w> <C-w><C-w>

nmap █ <Plug>AirlineSelectTab1
nmap ² <Plug>AirlineSelectTab2
nmap ³ <Plug>AirlineSelectTab3
nmap ⁴ <Plug>AirlineSelectTab4
nmap ½ <Plug>AirlineSelectTab5
nmap 😉 <Plug>AirlineSelectTab6
nmap 😃 <Plug>AirlineSelectTab7
nmap 😎 <Plug>AirlineSelectTab8
nmap ☹ <Plug>AirlineSelectTab9

" SAVING {{{1
noremap <LocalLeader>w :wall<CR>
" OPENING {{{1
noremap <expr> <LocalLeader>e ':e ' . expand("%:h") . '/'
noremap <expr> <LocalLeader>u ':e ' . expand("%:h") . '/../'
noremap <expr> <LocalLeader>U ':e ' . projectroot#guess() . '/'
noremap <LocalLeader>h :e ~/
noremap <LocalLeader>t :e /tmp/
noremap <LocalLeader>r :e /

" PASTING {{{1
noremap <silent> <Leader>p :put =@+<CR>
noremap <silent> <Leader>P :put! =@+<CR>
noremap <silent> <LocalLeader>p :put =@*<CR>
noremap <silent> <LocalLeader>P :put! =@*<CR>
" noremap! \p <C-r>=@*<CR>
" noremap! \P <C-r>=@*<CR>
noremap <Leader>y "+y
noremap <Leader>Y "+y$
noremap <LocalLeader>y "*y
noremap <LocalLeader>Y "*y$

" CREDENTIALS {{{1
nnoremap Q@ "=trim(system('git config --get user.email'))<CR>p
nnoremap QM "=trim(system('git config --get user.email'))<CR>p
nnoremap QN "=trim(system('git config --get user.name'))<CR>p

" SEARCHING {{{1
noremap / /\v\c
noremap ? ?\v\c
" search for visual selection			with \V you only have to escape \ and /
vnoremap * "zy/\V<C-r>=escape(@z, '\/')<CR><CR>
vnoremap # "zy?\V<C-r>=escape(@z, '\?')<CR><CR>
" open a Quickfix window for the last search
noremap <silent> <LocalLeader>/ :vimgrep /<C-r>//g %<CR>:copen<CR>

" REPLACING {{{1
noremap <LocalLeader>s :%s/\v
" replace every occurence
nnoremap Re :%s/\<<C-r><C-w>\>//gc<Left><Left><Left>
nnoremap RE :%s/\<<C-r><C-w>\>/<C-r><C-w>/gc<Left><Left><Left>
vnoremap Re "zy:%s/\V<C-r>=escape(@z, '\/')<CR>//gc<Left><Left><Left>
vnoremap RE "zy:%s/\V<C-r>=escape(@z, '\/')<CR>/<C-r>=escape(@z, '\/')<CR>/gc<Left><Left><Left>
" replace with lowercase and uppercase
nnoremap Ru :%s/\<<C-r><C-w>\>/\L&/gc<Left><Left><Left>
nnoremap RU :%s/\<<C-r><C-w>\>/\U&/gc<Left><Left><Left>
vnoremap Ru "zy:%s/\V<C-r>=escape(@z, '\/')<CR>/\L&/gc<Left><Left><Left>
vnoremap RU "zy:%s/\V<C-r>=escape(@z, '\/')<CR>/\U&/gc<Left><Left><Left>
" replace last search
noremap Rs :%s///gc<Left><Left><Left>
noremap RS :%s//<C-r>=escape(substitute(@z, '^\(\\.\)\+', '', ''), '\/')<CR>/gc<Left><Left><Left>

" FOLDING {{{1
noremap <expr> <CR> foldlevel('.') ? "za" : "\<CR>"
noremap <expr> <S-CR> &foldlevel ? "zM" : "zR"

" FUGITIVE SHORTCUTS {{{1
noremap <Leader>gc :Gcommit<CR>
noremap <Leader>gd :Gdiff<CR>
noremap <Leader>gs :Gstatus<CR>
noremap <Leader>gg :Ggrep 
noremap <Leader>gp :Gpush<CR>
" noremap <Leader>gpl :Gpull<CR>
" noremap <Leader>gw :Gwrite<CR>

" RUNNING {{{1
noremap RR :make<BAR>copen<CR>
noremap RI :IncVer<CR>

" OPERATOR MAPPINGS {{{1
onoremap q i"
onoremap Q i'
" whole buffer
onoremap ab :<C-u>normal! mzggVG<CR>`zzz

" DIGRAPHS {{{1
inoremap <C-\> <C-k>

" CALCULATE EQUATIONS {{{1
" inoremap <C-=> <C-o>"zyaW<End>=<C-r>=<C-r>z<CR>

" COC {{{1
" inoremap <silent><expr> <TAB>
"       \ pumvisible() ? "\<C-n>" :
"       \ <SID>check_back_space() ? "\<TAB>" :
"       \ coc#refresh()
" inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"
inoremap <silent><expr> <c-space> coc#refresh()

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" coc-git
" navigate chunks of current buffer
nmap [c <Plug>(coc-git-prevchunk)
nmap ]c <Plug>(coc-git-nextchunk)
" show chunk diff at current position
nmap <LocalLeader>gs <Plug>(coc-git-chunkinfo)
" show commit contains current position
nmap <LocalLeader>gc <Plug>(coc-git-commit)
" create text object for git chunks
omap ig <Plug>(coc-git-chunk-inner)
xmap ig <Plug>(coc-git-chunk-inner)
omap ag <Plug>(coc-git-chunk-outer)
xmap ag <Plug>(coc-git-chunk-outer)

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
	if (index(['vim','help'], &filetype) >= 0)
		execute 'h '.expand('<cword>')
	elseif (coc#rpc#ready())
		call CocActionAsync('doHover')
	else
		execute '!' . &keywordprg . " " . expand('<cword>')
	endif
endfunction

" Symbol renaming.
nmap <Leader>r <Plug>(coc-rename)

" Map function and class text objects
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
omap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap af <Plug>(coc-funcobj-a)
xmap ic <Plug>(coc-classobj-i)
omap ic <Plug>(coc-classobj-i)
xmap ac <Plug>(coc-classobj-a)
omap ac <Plug>(coc-classobj-a)


" " MAP JEDI SHORTCUTS TO YCM {{{1
" let g:jedi#goto_command = ""
" noremap <Leader>d :YcmCompleter GoToDefinition<CR>
" let g:jedi#goto_assignments_command = ""
" noremap <Leader>g :YcmCompleter GoToDeclaration<CR>
" let g:jedi#goto_definitions_command = ""
" let g:jedi#documentation_command = ""
" " noremap K :YcmCompleter GetDoc<CR>
" let g:jedi#usages_command = ""
" noremap <Leader>n :YcmCompleter GoToReferences<CR>
" let g:jedi#completions_command = "<C-Space>"
" let g:jedi#rename_command = "<Leader>r"

" " SUPPRESS LATEX SUITE MAPPINGS {{{1
" imap <nop> <Plug>IMAP_JumpForward
" nmap <nop> <Plug>IMAP_JumpForward
" vmap <nop> <Plug>IMAP_JumpForward
" vmap <nop> <Plug>IMAP_DeleteAndJumpForward
