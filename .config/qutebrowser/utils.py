import i3ipc
import passtodict


def get_height():
    i3 = i3ipc.Connection()
    out = next(out for out in i3.get_outputs() if out.active)
    return out.rect.height


def get_scroll_vars(one_scroll_px=40,   # one `j` or `k`
                    screen_h=get_height(),
                    borders_px=36,      # borders and bars
                    margin=10):
    """Get number of scroll steps and rest pixels for half page scroll"""
    # half the page minus a small margin
    to_scroll = (screen_h - borders_px)//2 - margin
    return divmod(to_scroll, one_scroll_px)


def get_coworkers_addr():
    """Get co-workers search address with credentials from pass filled"""
    try:
        mel = passtodict('work/mikro.mikroelektronika.cz')
    except passtodict.DecryptError:
        return ("http://intranet/search/Stranky/PeopleResults.aspx?k={}")

    return (f"http://{mel['login']}:{mel.PWD}@"
            "intranet/search/Stranky/PeopleResults.aspx?k={}")


if __name__ == '__main__':
    print(get_scroll_vars())
