import html
import json
import requests
from PyQt5.QtCore import QUrl

import qutebrowser
from qutebrowser.api import cmdutils
from qutebrowser.completion.models import completionmodel

from sugsearch.base import BaseCategory


class DDGCategory(BaseCategory):
    name = 'DuckDuckGo Suggestions'

    def get_suggestions(self, val):
        response = requests.get(
            url='https://duckduckgo.com/ac',
            params={'q': val}
            # 'callback': 'autocompleteCallback', 'kl': 'wt-wt',
            # '_': '1521193398252',
        )
        if response.status_code != 200:
            return
        try:
            data = json.loads(response.content)
            data[0]
        except Exception:
            return

        for i in data:
            yield i['phrase'],


def ddg_model(*args, info):
    model = completionmodel.CompletionModel(column_widths=(100,))
    model.add_category(DDGCategory())
    return model


@cmdutils.register(name='ddg')
@cmdutils.argument('query', completion=ddg_model)
def ddg(*query):
    """Search with DuckDuckGo."""
    query_str = html.escape(' '.join(query))
    qutebrowser.app.open_url(
        url=QUrl(f'https://duckduckgo.com?q={query_str}')
    )
