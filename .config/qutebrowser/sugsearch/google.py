import html
import json
import re
import requests
from PyQt5.QtCore import QUrl

import qutebrowser
from qutebrowser.api import cmdutils
from qutebrowser.completion.models import completionmodel

from sugsearch.base import BaseCategory


class GoogleCategory(BaseCategory):
    name = 'Google Suggestions'

    def get_suggestions(self, val):
        response = requests.get(
            url='https://www.google.com/complete/search',
            params={'client': 'psy-ab', 'q': val}
            # 'hl': 'en-US', 'gs_rn': '64', 'gs_ri': 'psy-ab', 'xhr': 't',
            # 'tok': 'pXwgeFs8dALTaY2s0zFqeA', 'cp': '1', 'gs_id': 'e',
        )
        if response.status_code != 200:
            return
        try:
            data = json.loads(html.unescape(response.content.decode('latin')))
        except Exception:
            return

        for item in data[1]:
            match = re.search(r'([^<]*)<b>([^<]*)</b>', item[0])
            if match:
                yield ''.join(match.groups()),


def google_model(*args, info):
    model = completionmodel.CompletionModel(column_widths=(100,))
    model.add_category(GoogleCategory())
    return model


@cmdutils.register(name='google')
@cmdutils.argument('query', completion=google_model)
def google(*query):
    """Search with Google."""
    # related=False, bg=False, tab=False, window=False, count=None,
    # secure=False, private=False

    # target = config.val.new_instance_open_target
    # # background = target in {'tab-bg', 'tab-bg-silent'}
    # win_id = mainwindow.get_window(via_ipc=True, force_target=target)
    # tabbed_browser = objreg.get('tabbed-browser', scope='window',
    #                             window=win_id)

    # log.init.debug("About to open URL: {}".format(url.toDisplayString()))
    # tabbed_browser.tabopen(url, background=background, related=False)
    # return win_id
    # curtab = tabbed_browser.widget.currentWidget()
    # curtab.openurl(
    #     QUrl(f'https://www.google.com/search?hl=en&q={query}'),
    #     newtab=False
    # )

    query_str = html.escape(' '.join(query))
    qutebrowser.app.open_url(
        url=QUrl(f'https://www.google.com/search?q={query_str}'),
    )
