-- 0	16	32	48	64	80	96	112	128	144	160	176	192	208	224	240	256					16*x
-- 8		16		32		64		128		256		512		1024	2048				8*2^(x+3)

-- (log2(speed) - 3)*32
byte = function(float)
	i = math.floor(float + 0.5)
	if i < 0 then
		return 0
	end
	if i > 255 then
		return 255
	end
	return i
end


rgb2hexstr = function(r, g, b)
	return string.format("#%02x%02x%02x", byte(r), byte(g), byte(b))
end


colored_span = function(val, f)
	return string.format('<span color="%s">%d</span>', rgb2hexstr(f(val)), val)
end


f = function(speed)
	r = (math.log(speed, 2) - 3)*32
	g = 255 - r
	b = 0
	return r, g, b
end
--print(rgb2hexstr(23.23544, 234.452316, 0))
print(colored_span(100, f))
