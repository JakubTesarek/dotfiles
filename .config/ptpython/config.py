# from prompt_toolkit.filters import ViInsertMode
# from prompt_toolkit.key_binding.key_processor import KeyPress
# from prompt_toolkit.keys import Keys
# from pygments import token as tok

# from ptpython.layout import CompletionVisualisation
from ptpython.repl import PythonRepl


__all__ = (
    'configure',
)


def configure(repl: PythonRepl):
    # Input
    repl.vi_mode = False
    repl.enable_mouse_support = False
    repl.confirm_exit = False

    # Display
    repl.show_signature = True
    repl.show_docstring = True
    repl.highlight_matching_parenthesis = True

    # Colors
    repl.use_code_colorscheme('gruvbox')
    repl.true_color = False


class LOL:
    """some docstring"""
    def __init__(self):
        if not False:
            print('lol\n')  # here we print lol
        else:
            raise Exception()

    @property
    def x(self, a=1.1e1):
        # TODO: not working
        from math import exp
        _x = int(self) + exp(a)
        return _x*'xxxxxx'   # here we return x


def roflcopter(total_frames=None):
    """ROFLs a copter"""
    from itertools import cycle, islice

    copter = ('{rofl1:^9}:LOL:{rofl2:^9}\n          _^___\n {el:^3}   __/   []'
              ' \\\n {lol:^3}===__        \\\n {el:^3}     \________)\n       '
              '   I   I\n         --------/\n')
    rofl = 'ROFL:ROFL'
    frames = [
        copter.format(rofl1=rofl, rofl2='', lol='LOL', el=''),
        copter.format(rofl1='', rofl2=rofl, lol='O', el='L'),
    ]

    yield from islice(cycle(frames), total_frames)


if __name__ == '__main__':
    for frame in roflcopter(5):
        print(frame)
