# vi: ft=config

# start a terminal
bindsym $m+Return exec $term
bindsym $m+Shift+Return exec $fm 2>/tmp/rifle.log

bindsym $m+p exec toggle-pidgin.py

bindsym $m+Shift+p exec $term pipes.sh -f30 -p2
bindsym $m+Shift+m exec $term cmatrix
bindsym $m+Shift+t exec $term tty-clock -cD
bindsym $m+Shift+o exec colorterm.sh

# kill focused window
bindsym $m+c kill

# bindsym $m+d exec dmenu_run
bindsym $m+r exec wofi --show run
# bindsym $m+Shift+r wofi --allow-images --show drun
bindsym $m+w exec DMENU_CMD="wofi --show dmenu" winfocus

# change focus
bindsym $m+h focus left
bindsym $m+j focus down
bindsym $m+k focus up
bindsym $m+l focus right
bindsym $m+Tab focus left

# alternatively, you can use the cursor keys:
bindsym $m+Left focus left
bindsym $m+Down focus down
bindsym $m+Up focus up
bindsym $m+Right focus right

# move focused window
bindsym $m+Shift+h move left
bindsym $m+Shift+j move down
bindsym $m+Shift+k move up
bindsym $m+Shift+l move right

# alternatively, you can use the cursor keys:
bindsym $m+Shift+Left move left
bindsym $m+Shift+Down move down
bindsym $m+Shift+Up move up
bindsym $m+Shift+Right move right

# enter fullscreen mode for the focused container
bindsym $m+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $m+v split vertical
bindsym $m+Shift+V split horizontal
# bindsym $m+s layout stacking
# bindsym $m+t layout tabbed
# bindsym $m+e layout toggle split

# toggle tiling / floating
# bindsym $m+Shift+space floating toggle

# change focus between tiling / floating windows
# bindsym $m+space focus mode_toggle

bindsym $m+space scratchpad show

# focus the parent container
# bindsym $m+a focus parent

# focus the child container
#bindsym $m+d focus child

# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $ws1 "1"
set $ws2 "2"
set $ws3 "3"
set $ws4 "4"
set $ws5 "5"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"

# switch to workspace
bindsym $m+1 workspace number $ws1
bindsym $m+2 workspace number $ws2
bindsym $m+3 workspace number $ws3
bindsym $m+4 workspace number $ws4
bindsym $m+5 workspace number $ws5
bindsym $m+6 workspace number $ws6
bindsym $m+7 workspace number $ws7
bindsym $m+8 workspace number $ws8
bindsym $m+9 workspace number $ws9

bindsym $m+Escape workspace back_and_forth

# move focused container to workspace
bindsym $m+Shift+1 move container to workspace number $ws1
bindsym $m+Shift+2 move container to workspace number $ws2
bindsym $m+Shift+3 move container to workspace number $ws3
bindsym $m+Shift+4 move container to workspace number $ws4
bindsym $m+Shift+5 move container to workspace number $ws5
bindsym $m+Shift+6 move container to workspace number $ws6
bindsym $m+Shift+7 move container to workspace number $ws7
bindsym $m+Shift+8 move container to workspace number $ws8
bindsym $m+Shift+9 move container to workspace number $ws9

# reload the configuration file
bindsym $m+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $m+Shift+r restart
# exit i3 (logs you out of your X session)
bindsym $m+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -B 'Yes, exit i3' 'i3-msg exit'"

# resize window (you can also use the mouse for that)
bindsym $m+Plus resize grow width 20 px or 20 ppt
bindsym $m+Minus resize shrink width 20 px or 20 ppt
bindsym $m+Equal resize set 50 %

mode "resize" {
	bindsym h resize shrink width 10 px or 10 ppt
	bindsym j resize grow height 10 px or 10 ppt
	bindsym k resize shrink height 10 px or 10 ppt
	bindsym l resize grow width 10 px or 10 ppt

	bindsym Left resize shrink width 10 px or 10 ppt
	bindsym Down resize grow height 10 px or 10 ppt
	bindsym Up resize shrink height 10 px or 10 ppt
	bindsym Right resize grow width 10 px or 10 ppt

	bindsym Return mode "default"
	bindsym Escape mode "default"
	bindsym $m+r mode "default"
}
bindsym $m+s mode "resize"

# Use pactl to adjust volume in PulseAudio.
set $refresh_i3status killall -SIGUSR1 i3status
bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ +10% && $refresh_i3status
bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ -10% && $refresh_i3status
bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute @DEFAULT_SINK@ toggle && $refresh_i3status
bindsym XF86AudioMicMute exec --no-startup-id pactl set-source-mute @DEFAULT_SOURCE@ toggle && $refresh_i3status

bindsym XF86AudioPlay exec playerctl play-pause
bindsym XF86AudioStop exec playerctl stop
bindsym XF86AudioPrev exec playerctl previous
bindsym XF86AudioNext exec playerctl next

# bindsym KP_Home exec notify-send ↖
# bindsym KP_Up exec notify-send ↑
# bindsym KP_Prior exec notify-send ↗
# bindsym KP_Left exec notify-send ←
# bindsym KP_Begin exec notify-send •
# bindsym KP_Right exec notify-send →
# bindsym KP_End exec notify-send ↘	
# bindsym KP_Down exec notify-send ↓
# bindsym KP_Next exec notify-send ↙
