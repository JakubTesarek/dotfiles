#!/bin/sh -eu

# Copyright (C) 2018  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

###
# Do something on thinkpad
#
# debs: ssh, coreutils
###

file="$(readlink -f "$@")"
url="http://rpi.home${file#/media}"

case $(basename "$0") in
	thinkpad.play.sh)
		command='mpv --really-quiet "'"$url"'"'
		;;
	thinkpad.copyurl.sh)
		# command='(printf '"'%s'"' "'"$url"'" | xclip -selection clipboard)'
		command='(printf '"'%s'"' "'"$url"'" | wl-copy)'
		;;
	# thinkpad.wget.sh)
	# 	target="/tmp/$(basename "$file")"
	# 	command='wget --background --quiet --directory-prefix=/tmp "'"$url"'"'
	# 	;;
	*)
		command='rifle "'"$url"'"'
		;;
esac

exec ssh pacholik@thinkpad.home '
export DISPLAY=:0
'"$command"' </dev/null >/dev/null 2>/dev/null &
'
