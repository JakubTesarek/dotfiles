#!/bin/sh -eu

# Copyright (C) 2017  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

tput civis
cleanup() {
	printf '\r\033[K'	# clear line
	tput cnorm
	exit "$1"
}
trap 'cleanup 1' INT TERM

c=0
p=0
while :; do
	[ $c -ge 400 ] && break
	printf ' '
	# printf '%s' "$chars" | while read -r char; do
	for char in ▏ ▎ ▍ ▌ ▋ ▊ ▉ █; do
		if [ $c -eq $p ]; then
			read -r _p
			p=${_p:-100}
			p=$(echo "($p*4+0.5)/1" | bc)
		fi

		printf '\b%s' "$char"
		sleep 0.0001
		c=$((c+1))
	done
done

cleanup 0
