#!/bin/sh -eu

# Copyright (C) 2020  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

###
# Today’s NASA Astronomy Picture of the Day as a wallpaper.
#
# You might append somthing like this to your crontab:
#     0 * * * * ~/bin/apod.sh
#
# or create file /etc/wicd/scripts/postconnect/apod.sh with this command:
#     su -l username -c .../apod.sh
#
# deps: wget, feh, jq, libnotify-bin, coreutils
###

exec 2>/tmp/apod.log

### set env vars so it works with cron
export DISPLAY=:0
export WAYLAND_DISPLAY=wayland-0
DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/$(id -u)/bus"
export DBUS_SESSION_BUS_ADDRESS

picdir="$HOME/pics/apod"
symlink="$HOME/pics/apod.jpg"
json="/tmp/apod.json"

### exit if we already have today’s json
if [ "$(jq --raw-output '.date' "$json")" = "$(date +%F)" ]; then
	jq --raw-output '.explanation' "$json"
	# feh --no-fehbg --bg-fill "$symlink"
	swaymsg "output * bg $symlink fill"
	false
fi

### dowlnload json
wget -qO "$json" 'https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY'
url=$(jq --raw-output --exit-status '.hdurl' "$json")
title=$(jq --raw-output '.title' "$json")
desc=$(jq --raw-output '.explanation' "$json" | fmt --uniform-spacing)

### dowlnload picture
mkdir -p "$picdir"
# oldpic="$(readlink "$symlink")"
newpic="$picdir/$(basename "$url")"
wget -cqO "$newpic" "$url"
ln -sf "$newpic" "$symlink"
# rm "$oldpic"

### set background
swaymsg "output * bg $symlink fill"
# if [ -n "$WAYLAND_DISPLAY" ]; then
# 	swaymsg "output * bg $symlink fill"
# else
# 	feh --no-fehbg --bg-fill "$symlink"
# fi
echo "$title"
echo "$desc"
notify-send --expire-time 20000 "$title" "$desc"
