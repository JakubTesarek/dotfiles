#!/bin/sh -eu

# Copyright (C) 2016  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

###
# Create deb package from python project, install it and upload to PyPI and mega.nz
#
# deps: python-stdeb, dpkg, passcred, twine, megatools, coreutils
###

echo() { printf '\n\e[4m%s\e[0m\n' "$*"; }
megals /Root > /dev/null 2>&1 &
export DEB_BUILD_OPTIONS=nocheck

python=python3
while :; do
	case "$1" in
		-2) python=python2; shift ;;
		-3) python=python3; shift ;;
		--pypi) pypi=1; shift ;;
		*) break
	esac
done

[ -n "$1" ] && cd "$1"

echo REMOVING dist AND deb_dist
[ -e dist ] && rm -r dist
[ -e deb_dist ] && rm -r deb_dist

echo CREATING PACKAGES
$python setup.py --command-packages=stdeb.command bdist_deb sdist bdist_wheel
echo INSTALLING
sudo dpkg -i deb_dist/*.deb

if [ "$pypi" ]; then
	echo UPLOADING TO PYPI
	passcred dev/pypi.org | (
		read -r user
		read -r pass
		twine upload -s -u"$user" -p"$pass" dist/*
	)
fi

echo WAITING FOR megals
wait
echo UPLOADING TO MEGA
megaput deb_dist/*.deb --path=/Root/debs
