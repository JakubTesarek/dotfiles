#!/bin/bash -eu

# Copyright (C) 2016  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

###
# Have only one gVim instance on every desktop.
#
# deps: gvim, wmctrl, grep
###

# clasify args
options=()
commands=()
filenames=()

for i in "$@"; do
	if [ "$_twohyphens" ]; then
		filenames+=("$i")
	else
		case "$i" in
			--) _twohyphens=1 ;;
			-*) options+=("$i") ;;
			+*) commands+=("$i") ;;
			*) filenames+=("$i") ;;
		esac
	fi
done

# desktop number will be used as servername
name=$(wmctrl -d | grep -oP '^\d+(?=\s*\*)')

# merge args
args=("--servername" "$name" "${options[@]}")
if [ -n "${commands[*]}${filenames[*]}" ]; then
	args+=("--remote-silent" "${commands[@]}" "${filenames[@]}")
fi

exec /usr/bin/gvim "${args[@]}"
