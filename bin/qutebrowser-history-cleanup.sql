#!/bin/sh
db="$HOME/.local/share/qutebrowser/history.sqlite"
since=$(date -d'2 years ago' +%s)
sed "s/_SINCE_/$since/; 1,/exit/d" "$0" | sqlite3 "$db"
exit "$?"

DELETE FROM History
WHERE (atime < _SINCE_)
OR url LIKE '%novinky%' OR url LIKE '%lidovky%' OR url LIKE '%duckduckgo%';

DELETE FROM CompletionHistory
WHERE (last_atime < _SINCE_)
OR url LIKE '%novinky%' OR url LIKE '%lidovky%' OR url LIKE '%duckduckgo%';
