#!/usr/bin/python3

# Copyright (C) 2018  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

"""Find all links on a webpage"""
# deps: beautifulsoup4, httplib2

import bs4
import httplib2
import urllib


def main(url):
    h = httplib2.Http()
    response, content = h.request(url)
    soup = bs4.BeautifulSoup(content, 'lxml')

    for tag in soup.find_all('a'):
        tag = tag   # type: bs4.element.Tag
        try:
            relative = tag.attrs['href']
        except KeyError:
            continue
        absolute = urllib.parse.urljoin(url, relative)
        print(absolute)


if __name__ == '__main__':
    import sys
    sys.exit(main(*sys.argv[1:]))
