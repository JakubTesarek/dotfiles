#!/bin/sh -eu

# Copyright (C) 2016  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

###
# Download a package from PyPI, create deb package, install it and upload to mega.nz
#
# deps: python-stdeb, dpkg, megatools, coreutils
###

echo() { printf '\n\e[4m%s\e[0m\n' "$*"; }
megals /Root > /dev/null 2>&1 &
export DEB_BUILD_OPTIONS=nocheck

python=python3
case "$1" in
	-2) python=python2; shift ;;
	-3) python=python3; shift ;;
esac

cd /tmp
echo REMOVING "$1"* and deb_dist
rm "$1"* || true
[ -e deb_dist ] && rm -r deb_dist

echo DOWNLOADING
archive=$($python /usr/bin/pypi-download "$1" | sed '/OK: /s///')
echo CREATING DEB
$python /usr/bin/py2dsc-deb "$archive"
echo INSTALLING
sudo dpkg -i deb_dist/*.deb

echo WAITING FOR megals
wait
echo UPLOADING TO MEGA
megaput deb_dist/*.deb --path=/Root/debs
