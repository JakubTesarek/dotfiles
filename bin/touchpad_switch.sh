#!/bin/sh -eu

# Copyright (C) 2016  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

###
# Toggle touchpad
#
# deps: xinput, sed, libnotify-bin
###

id=$(xinput list | sed -En '/[Tt]ouch[Pp]ad/s/^.*id=([0-9]+).*$/\1/p')
enabled=$(xinput list-props "$id" | sed -En '/Device Enabled/s/^.*([0-9]+)$/\1/p')

if [ "$enabled" = "0" ]; then
	xinput set-prop "$id" "Device Enabled" 1
	notify-send -i input-touchpad-symbolic "Touchpad is enabled."
else
	xinput set-prop "$id" "Device Enabled" 0
	notify-send -i input-touchpad-symbolic "Touchpad is disabled."
fi
