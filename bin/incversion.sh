#!/bin/sh -eu

# Copyright (C) 2019  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

###
# Increase version – on a line specified by the first argument,
#                    increase the last number
# examples:
#   incversion.sh /VERSION/ myfile.sh    # pattern
#   incversion.sh 5 myfile.sh            # line number

linespec="$1"
filename="$2"
# shellcheck disable=SC2016
sed -ri "$linespec"'s/(.*)([0-9]+)(.*)/echo "\1$((\2+1))\3"/e' "$filename"
sed -rn "$linespec"p "$filename"
