#!/bin/sh -eu

# Copyright (C) 2019  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

###
# Set scale factors and dpi
#
# deps: x11-xserver-utils, coreutils
###

export GDK_SCALE=1
# export QT_DEVICE_PIXEL_RATIO=1
# export QT_AUTO_SCREEN_SCALE_FACTOR=1

xrandr --output eDP-1 --scale 1x1 --dpi 96
echo 'URxvt.font: xft:DejaVu Sans Mono for Powerline:size=10' | xrdb
