#!/usr/bin/python3

# Copyright (C) 2018  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

"""Create kindle collection of xkcd strips"""
# deps: mkc, beautifulsoup4

import sys
import httplib2
from bs4 import BeautifulSoup

import mkc


def get_article(url, imgs=False, maths=True):
    context_element = ["div", {"id": "comic"}]

    h = httplib2.Http(mkc.CACHE_DIR, disable_ssl_certificate_validation=True)
    try:
        response, content = h.request(url)
    except Exception:
        print(url)
        raise
    # soup = BeautifulSoup(content)
    soup = BeautifulSoup(content, 'lxml')
    title = soup.find("title").contents[0]
    snip = soup.find(*context_element)

    img = snip.find("img")
    img["src"] = mkc.images[url, img.get("src", "")]

    content = """{snip}<p style="page-break-before:always">{alt}</p>""".format(
        snip=str(snip), alt=img["title"])
    return mkc.Article(title, content)


def main(*argv):
    r = range(2200, 2301)
    articles = [
        get_article("https://xkcd.com/{}".format(i), imgs=True, maths=False)
        for i in r
        # if i not in [2198]
    ]

    page = mkc.make_multiple_page(articles,
                                  name=f"xkcd_{r.start}-{r.stop - 1}",
                                  titles=True)
    print(mkc.html_to_mobi(page))


if __name__ == '__main__':
    main(sys.argv[1:])
