#!/bin/sh -e

megals /Root > /dev/null 2>&1 &

version="$(sed -n '/^VERSION=/s///p' create_ap)"
isnewer.sh 'create-ap' "$version"
dir="create_ap_$version"

mkdir -p "$dir/DEBIAN"

printf 'Package: create-ap
Version: %s
Section: network
Priority: optional
Architecture: all
Depends: bash, util-linux, procps | procps-ng, hostapd, iproute2, iw, dnsmasq
Recommends: haveged
Maintainer: Vojtěch Pachol <pacholick@gmail.com>
Description: This script creates a NATed or Bridged WiFi Access Point. 
' "$version" > "$dir/DEBIAN/control"

mkdir -p "$dir/usr/bin"
mkdir -p "$dir/lib/systemd/system"
mkdir -p "$dir/usr/share/bash-completion/completions"

cp create_ap "$dir/usr/bin/create_ap"
cp create_ap.service "$dir/lib/systemd/system"
cp bash_completion "$dir/usr/share/bash-completion/completions/create_ap"
dpkg -b "$dir"

deb="$dir.deb"
sudo dpkg -i "$deb"
wait
megaput "$deb" --path=/Root/debs
