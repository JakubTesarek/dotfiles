#!/bin/sh -e

megals /Root > /dev/null 2>&1 &

version="$(sed -En "/^[ \t]*version:/s/[^']*'([^']*)'.*/\1/p" meson.build)"
isnewer.sh 'wlrctl' "$version"
dir="waybar_$version"

set -- meson ninja-build scdoc libwayland-dev libxkbcommon-dev
sudo aptitude -y install "$@"
meson build
ninja -C build
# scdoc < wlrctl.1.scd | gzip > wlrctl.1.gz

mkdir -p "$dir/DEBIAN"

printf 'Package: wlrctl
Version: %s
Section: x11
Priority: optional
Architecture: amd64
Depends: libwayland-bin, libxkbcommon0
Maintainer: Vojtěch Pachol <pacholick@gmail.com>
Description: Command line utility for miscellaneous wlroots Wayland extensions.
' "$version" > "$dir/DEBIAN/control"

mkdir -p "$dir/usr/bin"
mkdir -p "$dir/usr/share/man/man1"
cp build/wlrctl "$dir/usr/bin/wlrctl"
# cp wlrctl.1.gz "$dir/usr/share/man/man1"
scdoc < wlrctl.1.scd | gzip > "$dir/usr/share/man/man1/wlrctl.1.gz"
dpkg -b "$dir"

deb=$dir.deb
sudo dpkg -i "$deb"
sudo aptitude -y markauto "$@"
wait
megaput "$deb" --path=/Root/debs
