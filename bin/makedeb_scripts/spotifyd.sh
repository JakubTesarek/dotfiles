#!/bin/sh -e

megals /Root > /dev/null 2>&1 &

version=$(sed -En '/^version = /s/[^"]*"([^"]*)".*/\1/p' Cargo.toml)
isnewer.sh 'spotifyd' "$version"
dir="spotifyd_$version"

set -- cargo libsdl2-dev
sudo aptitude -y install "$@"
cargo build --release --features="pulseaudio_backend"

mkdir -p "$dir/DEBIAN"

printf 'Package: spotifyd
Version: %s
Section: network
Priority: optional
Architecture: amd64
Depends: 
Maintainer: Vojtěch Pachol <pacholick@gmail.com>
Description: An open source Spotify client running as a UNIX daemon. Spotifyd streams music just like the official client, but is more lightweight and supports more platforms. Spotifyd also supports the Spotify Connect protocol which makes it show up as a device that can be controlled from the official clients.
' "$version" > "$dir/DEBIAN/control"

mkdir -p "$dir/usr/bin"
mkdir -p "$dir/lib/systemd/system"

cp target/release/spotifyd "$dir/usr/bin/"
cp contrib/spotifyd.service "$dir/lib/systemd/system/"
dpkg -b "$dir"

deb="$dir.deb"
sudo dpkg -i "$deb"
sudo aptitude -y markauto "$@"
wait
megaput "$deb" --path=/Root/debs
