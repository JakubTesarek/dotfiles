#!/bin/sh -e

megals /Root > /dev/null 2>&1 &

version="$(git describe --long)"
isnewer.sh 'hed' "$version"
dir="hed_$version"

# set -- libqrencode-dev
# sudo aptitude -y install "$@"
make

mkdir -p "$dir/DEBIAN"

printf 'Package: hed
Version: %s
Section: utils
Priority: optional
Architecture: amd64
Maintainer: Vojtěch Pachol <pacholick@gmail.com>
Description: vim like hex editor
' "$version" > "$dir/DEBIAN/control"

mkdir -p "$dir/usr/bin"
cp hed "$dir/usr/bin/hed"
dpkg -b "$dir"

deb=$dir.deb
sudo dpkg -i "$deb"
# sudo aptitude -y markauto "$@"
wait
megaput "$deb" --path=/Root/debs
