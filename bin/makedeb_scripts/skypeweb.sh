#!/bin/sh -e

megals /Root > /dev/null 2>&1 &

version=$(sed -En '/#define SKYPEWEB_PLUGIN_VERSION/s/[^"]*"([^"]*)".*/\1/p' skypeweb/libskypeweb.h)
isnewer.sh 'skypeweb' "$version"

set -- libpurple-dev libjson-glib-dev cmake gcc
sudo aptitude -y install "$@"
mkdir -p skypeweb/build
cd skypeweb/build/
cmake ..
cpack

sudo dpkg -i -- *.deb
rm -- *.deb

sudo aptitude -y markauto "$@"
wait
megaput ./*.deb --path=/Root/debs
