#!/bin/sh -e

megals /Root > /dev/null 2>&1 &

version="$(sed -n '/BITFETCH_VERSION = /s///p' Makefile)"
isnewer.sh 'bitfetch' "$version"
dir="bitfetch_$version"

set -- libxinerama-dev
sudo aptitude -y install "$@"
make

mkdir -p "$dir/DEBIAN"

printf 'Package: bitfetch
Version: %s
Section: utils
Priority: optional
Architecture: amd64
Depends: libxinerama1
Maintainer: Vojtěch Pachol <pacholick@gmail.com>
Description: simple cli system information tool written in C
' "$version" > "$dir/DEBIAN/control"

mkdir -p "$dir/usr/bin"
cp bitfetch "$dir/usr/bin/bitfetch"
dpkg -b "$dir"

deb=$dir.deb
sudo dpkg -i "$deb"
sudo aptitude -y markauto "$@"
wait
megaput "$deb" --path=/Root/debs
