#!/bin/sh -eu

# Copyright (C) 2018  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

###
# Tell me what time I got into the office
#
# deps: passcred, wget, sed
###

passcred work/mikro.mikroelektronika.cz | (
	read -r user
	read -r pass
	url="http://$user:$pass@intranet/SitePages/Home.aspx"
	wget -qO- "$url" | sed -En '/Př&#237;chod/s|.*<td>([0-9 .:]+)</td>.*|\1|p'
)
