#!/usr/bin/python3

# Copyright (C) 2018  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

# deps: passtodict, requests-ntlm

import sys
import passtodict
import requests
from requests_ntlm import HttpNtlmAuth
import bs4


def download():
    cred = passtodict('work/mikro.mikroelektronika.cz')
    url = "http://intranet/SitePages/Home.aspx"
    response = requests.get(
        url,
        auth=HttpNtlmAuth(f"{cred['workgroup']}\\{cred.LOGIN}", cred.PWD))
    return response.text


def find_table(text: str):
    soup = bs4.BeautifulSoup(text, 'lxml')
    td = soup.find('td', text='Osobní číslo')
    return td.parent.parent


def gen_dict(table: bs4.element.Tag):
    dct = {}
    for tr in table.find_all('tr', recursive=False):
        tdk, tdv = tr.find_all('td', recursive=False, limit=2)
        dct[tdk.text] = tdv.text
    return dct


def main():
    text = download()
    table = find_table(text)
    dct = gen_dict(table)
    from pprint import pprint
    pprint(dct)


if __name__ == '__main__':
    main(*sys.argv[1:])
