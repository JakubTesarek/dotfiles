#!/usr/bin/python3

# Copyright (C) 2020  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

import sys
# import asyncio
import i3ipc
import i3ipc.aio
import subprocess
import psutil
import signal
import time
# import logging


def main(already_launched=False):
    # i3 = await i3ipc.aio.Connection().connect()

    conn = i3ipc.Connection()
    tree = conn.get_tree()

    pidgin_set = (
        set(tree.find_classed('Pidgin')) and
        set(tree.find_titled('Buddy List'))
    )

    if not pidgin_set and not already_launched:
        subprocess.Popen(['pidgin'])
        waybar_p = next(p for p in psutil.process_iter(['name']) if
                        p.info['name'] == 'waybar')
        waybar_p.send_signal(signal.SIGRTMIN+9)
        time.sleep(1)
        return main(already_launched=True)

    pidgin = pidgin_set.pop()

    if pidgin.parent.name == '__i3_scratch':
        pidgin.command('scratchpad show')
        pidgin.command('floating disable')
        pidgin.command('resize set 30 %')
    else:
        pidgin.command('floating enable')
        pidgin.command('move scratchpad')


if __name__ == '__main__':
    # asyncio.run(main(*sys.argv[1:]))
    main(*sys.argv[1:])
